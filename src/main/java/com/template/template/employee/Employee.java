package com.template.template.employee;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "emp_id")
    private int id;

    @Column(name = "emp_name")
    private String name;

    @Column(name = "emp_age")
    private int age;

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

}
