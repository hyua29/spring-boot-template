package com.template.template.employee;


import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called employeeRepo
// CRUD refers Create, Read, Update, Delete
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}